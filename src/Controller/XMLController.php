<?php

namespace Drupal\simple_views_xml_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class XMLController.
 */
class XMLController extends ControllerBase {

  /**
   * Xml_generate.
   *
   * @return string
   *   Return XML.
   */
  public function xml_generate($nid) {
    // The machine name of view
    $viewname = 'xml_export';

    // Get the view
    $view = \Drupal\views\Views::getView($viewname);

    if (!$view) {
      // The view could not be loaded. Add a watchdog message
      \Drupal::logger('custom_xml_export')
        ->error('View @view not found', ['@view' => $viewname]);
      return FALSE;
    }

    // Set the display machine name
    $view->setDisplay('default');

    // Set the argument
    $view->setArguments([$nid]);

    // Execute the view
    $view->preview();

    // Set the results
    $results = $view->result;

    // Set the response
    $response = new Response();
    $response->setContent('');

    if ($results) {

      $response = new Response();
      $response->headers->set('Content-Type', 'application/xml');

      foreach ($results as $rid => $row) {
        foreach ($view->field as $fid => $field) {
          $values[$rid][$fid] = $field->getValue($row);
        }
      }

      $xml = new \SimpleXMLElement('<krpano/>');

      $xml_image = $xml->addChild('image');
      $xml_image->addAttribute('stereo', 'false');

      $xml_sphere = $xml_image->addChild('sphere');
      $xml_sphere->addAttribute('url',
        '/data/1/babc231a-796d-4c9e-bbdc-89741e0dde27.p.m.city1.jpg');

      $xml_view = $xml->addChild('view');
      $xml_view->addAttribute('hlookat', 0);
      $xml_view->addAttribute('fovmin', 90);
      $xml_view->addAttribute('fovmax', 90);

      // TEST ONLY
      $title = $values[0]['title'];
      $xml_title = $xml->addChild('title', $title);
      //

      $out_xml = $xml->asXML();
      $response->setContent($out_xml);
    }
    return $response;
  }
}
